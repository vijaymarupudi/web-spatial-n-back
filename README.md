Web spatial n-back
==================

Demo
----

[Click here](https://vijaymarupudi.gitlab.io/uwlatl-experiment/task.html)

Building
--------

In the folder of the project, run:

```javascript
npm install
NODE_ENV=production npx webpack -p
```

Then copy over `task.html` and the `dist` directory to a static web server.

Configuration
-------------

Edit the settings object in `src/index.js`

Usage
-----

The task page sends a `window.opener` message with the trialData after it is completed. In order to use it, the experiment page (Qualtrics or Mechanical Turk) needs to open the experiment in a link that would open a new tab i.e. `target="_blank"`.

Then, add an event listener on the experiment page to proceed after receiving an image. Example for Qualtrics:


```javascript
Qualtrics.SurveyEngine.addOnload(function () {
  var qual = this
  // Prevent next button when the page loads
  qual.disableNextButton()

  // Listen for message
  window.addEventListener('message', (e) => {
    qual.enableNextButton()
    qual.clickNextButton()
  })
})
```

If other labs are interesting in using this, please feel free to contact me at vijay at vijaymarupudi dot com.
