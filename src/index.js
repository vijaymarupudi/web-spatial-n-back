import m from "mithril";
import { times } from "lodash-es";

import screenfull from "screenfull";

const settings = {
  n: 3,
  trialCount: 70
};

window.settings = settings

const state = {
  list: [],
  time: null,
  trialData: [],
  message: null,
  warning: false,
  isDelayPeriod: false,
  delayDuration: 250,
  displayEndingScreen: false,
  intro: true
};

window.state = state;

// ---------------------------------------------

const calc = {
  get mostRecentListItem() {
    return state.list[state.list.length - 1];
  },
  get currentNBackIndex() {
    const currentIndex = state.list.length - 1;
    const nBackIndex = currentIndex - settings.n;
    if (nBackIndex >= 0) {
      return state.list[nBackIndex];
    }
  },
  get nextItemNBackIndex() {
    const nextItemIndex = state.list.length;
    const nBackIndex = nextItemIndex - settings.n;
    if (nBackIndex >= 0) {
      return state.list[nBackIndex];
    }
  },
  get score() {
    const correctTrials = state.trialData.filter(v => v.correct);
    return correctTrials.length / state.trialData.length;
  },
  get wasLastTrialCorrect() {
    if (state.trialData.length !== 0) {
      return state.trialData[state.trialData.length - 1].correct;
    }
  },
  blankIndex: (3 ** 2 - 1) / 2,
  upperBoundIndex: 3 ** 2 - 1
};

window.calc = calc;

// ------------------------------------------------

function delay(time) {
  return new Promise((resolve, reject) => {
    setTimeout(resolve, time);
  });
}

// ------------------------------------------------

function logTrial(keyPressed, duration) {
  const trial = {
    duration: duration,
    currentValue: calc.mostRecentListItem,
    nBackValue: calc.currentNBackIndex,
    timeOfTrialEnd: Date.now(),
    keyPressed: keyPressed
  };

  if (trial.nBackValue != null) {
    if (keyPressed === "y") {
      trial.correct = trial.nBackValue === trial.currentValue;
    } else if (keyPressed === "n") {
      trial.correct = trial.nBackValue !== trial.currentValue;
    }
  }

  state.trialData.push(trial);
}

function startStopwatch() {
  state.time = window.performance.now();
}

function stopStopwatch() {
  const oldTime = state.time;
  const currentTime = window.performance.now();
  const duration = currentTime - oldTime;
  state.time = currentTime;
  return duration;
}

// -------------------------------------------------

function fiftyFifty(val1, val2) {
  const randomNumber = Math.random();
  if (randomNumber < 0.5) return val1;
  else return val2;
}

function randIntButBlank() {
  const randInt = Math.round(Math.random() * calc.upperBoundIndex);
  if (randInt === calc.blankIndex) return randIntButBlank();
  else return randInt;
}

function randListItem() {
  const randomIndex = randIntButBlank();
  const nBackIndex = calc.nextItemNBackIndex;
  const selectedValue = fiftyFifty(randomIndex, nBackIndex);
  if (nBackIndex == null) {
    return randomIndex;
  } else {
    return selectedValue;
  }
}

function indexToCoordinates(index) {
  const row = Math.floor(index / 3);
  const col = index % 3;
  return [row, col];
}

function generateTableArray() {
  // Making blank array
  const tableArray = times(3, idx => times(3, () => false));
  // Filling blank index
  tableArray[1][1] = "x";

  if (state.isDelayPeriod) return tableArray; // If delay, return blank array

  // Filling the intended block
  if (!(calc.mostRecentListItem == null)) {
    const [row, col] = indexToCoordinates(calc.mostRecentListItem);
    tableArray[row][col] = true;
  }
  return tableArray;
}

const Table = {
  view({ attrs }) {
    const table = attrs.table;
    return m("table.table.nback-table", [
      m("tbody", [
        table.map(row =>
          m("tr", [
            row.map(col => {
              let tdClass;
              if (col === true) {
                tdClass = "yellow";
              } else {
                tdClass = "";
              }
              const itemText = typeof col !== "boolean" ? col : null;
              return m(
                "td.experiment-block.nback-td",
                { class: tdClass },
                itemText
              );
            })
          ])
        )
      ])
    ]);
  }
};

const Beginning = {
  view() {
    return m(
      "div.hero.is-fullheight",
      m(
        "div.hero-body",
        m("div.content", [
          m("h1", "Please click the button to begin"),
          m(
            "div",
            m(
              "button",
              {
                onclick: () => {
                  if (screenfull.enabled) {
                    screenfull.request();
                    state.intro = false;
                    startUpNBack();
                  }
                }
              },
              "Click here"
            )
          )
        ])
      )
    );
  }
};

const App = {
  view() {
    if (state.intro) return m(Beginning);

    const displayedElements = [];

    if (state.warning) {
      displayedElements.push(
        m("div.notification.is-warning", "y or n keys only.")
      );
      state.warning = false;
    }

    // Feedback element
    if (calc.wasLastTrialCorrect === true) {
      displayedElements.push(m("div.notification.is-success", "Correct!"));
    } else if (calc.wasLastTrialCorrect === false) {
      displayedElements.push(m("div.notification.is-danger", "Wrong!"));
    }

    if (state.message) {
      displayedElements.push(m("div.notification", state.message));
    }

    if (state.displayEndingScreen) {
      return m("div", [
        m(
          "div.hero-is-fullheight",
          m("div.hero-body", [
            m("div.container", [m("h1.title", "Please wait to be redirected!")])
          ])
        )
      ]);
    }
    return m("div", [
      m(
        "div.container.section.nback-table-container",
        m(Table, { table: generateTableArray() })
      ),
      m("div.section.container", displayedElements)
    ]);
  }
};

// Event loop

function progressRegularTrial(key) {
  if (state.trialData.length > settings.trialCount) {
    state.displayEndingScreen = true;
    m.redraw();
    if (window.opener) {
      const dataToSend = {
        settings: settings,
        state: state
      };
      window.opener.postMessage(dataToSend, "*");
      delay(1000).then(() => {
        window.close();
      });
    }
    return;
  }

  if (key === "y" || key === "n") {
    const duration = stopStopwatch();
    logTrial(key, duration);
    state.list.push(randListItem());
    state.isDelayPeriod = true;
    m.redraw();
    delay(state.delayDuration).then(() => {
      state.isDelayPeriod = false;
      addkeyEventLoop();
      startStopwatch();
      m.redraw();
    });
  } else {
    state.warning = true;
    addkeyEventLoop();
    m.redraw();
  }
}

function trialResponse(e) {
  progressRegularTrial(e.key);
}

function addkeyEventLoop() {
  document.addEventListener("keyup", trialResponse, { once: true });
}

async function startUpNBack() {
  for (let i = 0; i < settings.n + 1; i++) {
    if (i === 0) {
      state.message = "Loading experiment...";
      m.redraw();
    }
    state.isDelayPeriod = true;
    m.redraw();
    await delay(state.delayDuration);
    state.isDelayPeriod = false;
    m.redraw();
    await delay(2000);
    state.list.push(randListItem());
    state.message = `Square number: ${i + 1}`;
    m.redraw();
  }

  // Final delay before final part

  state.isDelayPeriod = true;
  m.redraw();
  await delay(state.delayDuration);
  state.isDelayPeriod = false;
  m.redraw();
  state.message = "You can proceed!";
  m.redraw();

  // Add key event loop and start stopwatch
  startStopwatch();
  addkeyEventLoop();

  // Show the message for 2 seconds before removing it
  await delay(2000);
  state.message = null;
  m.redraw();
}

m.mount(document.getElementById("app"), App);
